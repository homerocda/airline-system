﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// The type of the ticket (Business, Economy, Premium Economy, First Class, etc...)
    /// </summary>
    public class TicketType
    {
        private string name;
        private string description;
        private List<SeatCategory> seatCategories;

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public TicketType()
        {
        }

        /// <summary>
        /// Constructor with Parameters.
        /// </summary>
        /// <param name="name">Name of this ticket type.</param>
        /// <param name="description">Description of this ticket type.</param>
        /// <param name="seatCategories">Categories appliable to this ticket type.</param>
        public TicketType(string name, string description, List<SeatCategory> seatCategories)
        {
            this.name = name;
            this.description = description;
            this.seatCategories = seatCategories;
        }

        /// <summary>
        /// Gets or sets the name of this ticket type.
        /// </summary>
        public string Name { get => name; set => name = value; }

        /// <summary>
        /// Gets or sets the description of this ticket type.
        /// </summary>
        public string Description { get => description; set => description = value; }

        /// <summary>
        /// Gets or sets the seat categories applicable to this ticket type.
        /// </summary>
        public List<SeatCategory> SeatCategories { get => seatCategories; set => seatCategories = value; }
    }
}