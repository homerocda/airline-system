﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This class represents a Pilot of the Airline. It inherits from Employee and realizes
    /// the Polymorphism.
    /// </summary>
    public class Pilot : Employee
    {
        private const string JOB_TITLE = "PILOT";
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Pilot()
        {
        }

        /// <summary>
        /// Constructor with parameters.
        /// </summary>
        /// <param name="firstName">pilot first name</param>
        /// <param name="lastName">pilot last name</param>
        /// <param name="email">pilot email</param>
        /// <param name="phone">pilot phone</param>
        /// <param name="birthDate">pilot birth date</param>
        /// <param name="employeeId">pilot id</param>
        /// <param name="hiringDate">pilot hiring date</param>
        /// <param name="leavingDate">pilot leave date</param>
        public Pilot(string firstName, string lastName, string email, string phone, DateTime birthDate, string employeeId, DateTime hiringDate, DateTime leavingDate) : base(firstName, lastName, email, phone, birthDate, employeeId, JOB_TITLE, hiringDate, leavingDate)
        {
        }
    }
}