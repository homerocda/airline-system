﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// The interface for Component part of the Decorator pattern of the ticket.
    /// </summary>
    public interface ITicket
    {
        /// <summary>
        /// Calculates total ticket price.
        /// </summary>
        /// <returns>Returnst total price</returns>
        decimal CalculatePrice();
        /// <summary>
        /// Gets the cancellable hours for the ticket.
        /// </summary>
        /// <returns>The cancellable time before flight departure.</returns>
        DateTime GetCancellableHours();
        /// <summary>
        /// Cancels the ticket.
        /// </summary>
        void Cancel();
    }
}