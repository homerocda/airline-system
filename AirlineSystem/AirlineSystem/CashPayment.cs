﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This class represents a cash payment. It inherits from
    /// Payment and realizes the Polymorphism.
    /// </summary>
    public class CashPayment : Payment
    {
        /// <summary>
        /// This method does a cash payment action
        /// </summary>
        /// <param name="amount">amount of cash</param>
        /// <returns>true if payment is successful and vice versa.</returns>
        public override bool Pay(int amount)
        {
            throw new NotImplementedException();
        }
    }
}