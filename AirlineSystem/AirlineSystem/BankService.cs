﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// Service represents bank service payment.
    /// This class serves as adaptee in Adapter pattern.
    /// </summary>
    public class BankService
    {
        private string airlineBankInformation;
        /// <summary>
        /// Constructor with airline bank information
        /// </summary>
        /// <param name="airlineBankInformation">A JSON string represents airline bank information</param>
        public BankService(string airlineBankInformation)
        {
            this.airlineBankInformation = airlineBankInformation;
        }
        /// <summary>
        /// Do the bank transfer payment with amount and bank information
        /// </summary>
        /// <param name="amount">Amount to pay</param>
        /// <param name="bankInformation">JSON string represent customer's bank information</param>
        /// <returns></returns>
        public bool Pay(int amount, string bankInformation)
        {
            Console.WriteLine("Do payment by bank transfer.");
            return true;
        }
    }
}