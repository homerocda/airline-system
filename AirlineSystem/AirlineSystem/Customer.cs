﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This class represents a customer of the airline.
    /// It can create, update, view, cancel and pay reservations.
    /// It can view it's flight history.
    /// It inherits from Person and realizes polymorphism.
    /// </summary>
    public class Customer : Person
    {
        private string accountNumber;
        private string username;
        private string password;
        private List<Reservation> reservations;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Customer()
        {
        }

        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="firstName">customer first name</param>
        /// <param name="lastName">customer last name</param>
        /// <param name="email">customer email</param>
        /// <param name="phone">customer phone</param>
        /// <param name="birthDate">customer birth date</param>
        /// <param name="accountNumber">customer account number</param>
        /// <param name="username">customer username</param>
        /// <param name="password">customer password</param>
        public Customer(string firstName, string lastName, string email, string phone, DateTime birthDate, string accountNumber, string username, string password) : base(firstName, lastName, email, phone, birthDate)
        {
            this.accountNumber = accountNumber;
            this.username = username;
            this.password = password;
        }

        /// <summary>
        /// Gets or sets the account number
        /// </summary>
        public string AccountNumber { get => accountNumber; set => accountNumber = value; }
        /// <summary>
        /// gets/sets the username
        /// </summary>
        public string Username { get => username; set => username = value; }
        /// <summary>
        /// gets/sets the password
        /// </summary>
        public string Password { get => password; set => password = value; }
        /// <summary>
        /// gets/sets the reservation list
        /// </summary>
        public List<Reservation> Reservations { get => reservations; set => reservations = value; }

        /// <summary>
        /// Creates a new reservation.
        /// </summary>
        /// <returns>the new reservation</returns>
        public Reservation CreateReservation()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Cancels a particular reservation.
        /// </summary>
        /// <param name="reservation"></param>
        public void CancelReservation(Reservation reservation)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Pays for a reservation with the given payment class.
        /// </summary>
        /// <param name="reservation">the reservation to pay for</param>
        /// <param name="payment">the payment method</param>
        public void PayReservation(Reservation reservation, Payment payment)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the customer flight history
        /// </summary>
        /// <returns>List of flights booked by this customer</returns>
        public List<Flight> GetFlightHistory()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the list of tickets from a particular reservation identified by
        /// the Reservation Code.
        /// </summary>
        /// <param name="reservationCode">The reservation code</param>
        /// <returns>The tickets in that reservation</returns>
        public List<Ticket> ShowTickets(string reservationCode)
        {
            throw new NotImplementedException();
        }
    }
}