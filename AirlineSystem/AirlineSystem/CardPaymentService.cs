﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This class represents an adapter to Payment class to make payment for creditcard.
    /// </summary>
    public class CardPaymentService
    {
        /// <summary>
        /// Connect to payment service.
        /// </summary>
        public void Connect()
        {
            Console.WriteLine("Do the connection");
        }

        /// <summary>
        /// Pay the amount with card information
        /// </summary>
        /// <param name="amount">amount you need to pay</param>
        /// <param name="cardInformation">information on card, represents in JSON string</param>
        /// <returns>status of service call</returns>
        public bool Pay(int amount, string cardInformation)
        {
            Console.WriteLine("Do the payment");
            return true;
        }
    }
}