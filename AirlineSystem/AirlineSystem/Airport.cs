﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// Represents an Airport an Airline can operate.
    /// </summary>
    public class Airport
    {
        private string name;
        private string city;
        private string country;
        private string iataCode;

        /// <summary>
        /// Default constructor
        /// </summary>
        public Airport()
        {
        }

        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="name">Airport name</param>
        /// <param name="city">City the airport servers</param>
        /// <param name="country">Country the city lies in</param>
        /// <param name="iataCode">IATA code for the Airport</param>
        public Airport(string name, string city, string country, string iataCode)
        {
            Name = name;
            City = city;
            Country = country;
            IATACode = iataCode;
        }

        /// <summary>
        /// Gets / sets the name of the airport
        /// </summary>
        public string Name { get => name; set => name = value; }

        /// <summary>
        /// gets / sets the city this airport serves
        /// </summary>
        public string City { get => city; set => city = value; }

        /// <summary>
        /// Gets / sets the country of this airport
        /// </summary>
        public string Country { get => country; set => country = value; }

        /// <summary>
        /// Gets / sets the IATA code of the Airport
        /// </summary>
        public string IATACode { get => iataCode; set => iataCode = value; }
    }
}