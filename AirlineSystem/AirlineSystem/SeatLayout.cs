﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This class represents the seat layout of a Airplane Model.
    /// </summary>
    public class SeatLayout
    {
        private List<SeatRow> rows;
        /// <summary>
        /// Gets the number of seats available in this layout.
        /// </summary>
        public int SeatCount {
            get
            {
                return rows.Sum(s => s.Seats.Count);
            }
        }

        /// <summary>
        /// Gets or Sets the seat rows in this Layout.
        /// </summary>
        public List<SeatRow> SeatRows { get => rows; set => rows = value; }

        /// <summary>
        /// We use the Grasp Creator pattern to create the Seat Rows based 
        /// in a jagged array of the seat categories of this plane.
        /// </summary>
        /// <param name="seats">A jagged array with the seat layout of the plane</param>
        public SeatLayout(SeatCategory[][] seats)
        {
            throw new NotImplementedException();
        }

    }
}