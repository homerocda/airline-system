﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This class represents a single airplane in an Airline Fleet.
    /// </summary>
    public class Airplane
    {
        private SeatLayout seatLayout;
        private Airline airline;
        private AirplaneModel model;
        private string callsign;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Airplane()
        {
        }

        /// <summary>
        /// Constructor with parameters.
        /// </summary>
        /// <param name="airline">The airline this airplane belongs to.</param>
        /// <param name="model">The model of this airplane.</param>
        /// <param name="callsign">The callsign used by this airplane.</param>
        /// <param name="seatLayout">The seat layout of this airplane.</param>
        public Airplane(Airline airline, AirplaneModel model, string callsign, SeatLayout seatLayout)
        {
            Airline = airline;
            Model = model;
            Callsign = callsign;
            SeatLayout = seatLayout;
        }

        /// <summary>
        /// The Airline this Airplane belongs to.
        /// </summary>
        public Airline Airline
        {
            get => airline;
            set => airline = value;
        }

        /// <summary>
        /// The Model of this Airplane
        /// </summary>
        public AirplaneModel Model
        {
            get => model;
            set => model = value;
        }

        /// <summary>
        /// The Callsign that identifies this airplane
        /// to the flight controller.
        /// </summary>
        public string Callsign
        {
            get => callsign;
            set => callsign = value;
        }

        /// <summary>
        /// The seat layout used by this particular plane.
        /// </summary>
        public SeatLayout SeatLayout
        {
            get => seatLayout;
            set => seatLayout = value;
        }
    }
}