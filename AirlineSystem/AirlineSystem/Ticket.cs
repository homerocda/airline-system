﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// Represent a ticket in flight system.
    /// Decorator pattern: It can be decorated through an <see cref="InsurableTicket"/>
    /// Observer pattern: Server as observer in Observer pattern
    /// </summary>
    public class Ticket : ITicket, ITicketNotifier
    {
        private Flight flight;
        private Seat seat;
        private Passenger passenger;
        private TicketType type;
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Ticket()
        {

        }

        /// <summary>
        /// Constructor with flight, seat, passenger and type
        /// </summary>
        /// <param name="flight">Flight of that ticket</param>
        /// <param name="seat">Seat that ticket own</param>
        /// <param name="passenger">passenger on that ticket</param>
        /// <param name="type">Type of ticket</param>
        public Ticket(Flight flight, Seat seat, Passenger passenger, TicketType type)
        {
            Flight = flight;
            Passenger = passenger;
            Seat = seat;
            Type = type;
        }
        /// <summary>
        /// Flight property. Represent a flight
        /// </summary>
        public Flight Flight
        {
            get => flight; set => flight = value;
        }
        /// <summary>
        /// Seat property. Represent a seat in flight
        /// </summary>
        public Seat Seat
        {
            get => seat;
            set
            {
                seat = value;
            }
        }
        /// <summary>
        /// This method calculates price
        /// </summary>
        /// <returns>return calculated price</returns>
        public decimal CalculatePrice()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Passenger property. Represent a passenger in ticket.
        /// </summary>
        public Passenger Passenger
        {
            get => passenger;
            set => passenger = value;
        }
        /// <summary>
        /// Represent type of Ticket (Business, Economy, Premium Economy)
        /// </summary>
        public TicketType Type
        {
            get => type;
            set => type = value;
        }

        /// <summary>
        /// This method cancel the ticket.
        /// This will trigger a Refund if applicable.
        /// </summary>
        public void Cancel()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method get cancellableHours period.
        /// </summary>
        /// <returns>A DateTime instance represent deadline of cancellable hours</returns>
        public DateTime GetCancellableHours()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Observer pattern: Observable will call this method to notify about flight route status.
        /// </summary>
        /// <param name="flight">changed flight</param>
        public void Notify(Flight flight)
        {
            throw new NotImplementedException();
        }
    }
}