﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This class represents a bank transfer payment.
    /// Adapter pattern: This class serve as adapter to adapt bank transfer service to Payment interface.
    /// It inherits from Payment and realizes the Polymorphism.
    /// </summary>
    public class BankTransferPayment : Payment
    {
        private BankService bankService;
        private string bankInformation;
        /// <summary>
        /// Default constructor
        /// </summary>
        public BankTransferPayment()
        {
        }
        /// <summary>
        /// Constructor with bank information
        /// </summary>
        /// <param name="bankInformation">A JSON string represents bank information</param>
        public BankTransferPayment(string bankInformation)
        {
            this.bankInformation = bankInformation;
        }
        /// <summary>
        /// This property represents bank service and serves as adaptee in Adapter pattern.
        /// </summary>
        public BankService BankService
        {
            get => bankService;
            set => bankService = value;
        }
        /// <summary>
        /// This method does payment with amount.
        /// </summary>
        /// <param name="amount">Amount to pay</param>
        /// <returns>True if payment is successful and vice versa.</returns>
        public override bool Pay(int amount)
        {
            return bankService.Pay(amount, bankInformation);
        }
    }
}