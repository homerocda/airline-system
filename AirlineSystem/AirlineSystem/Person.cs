﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirlineSystem
{
    /// <summary>
    /// This class holds all the personal data of any
    /// type of person interacting with the system.
    /// We use the polimorphism pattern here to represent
    /// the different roles different people play in
    /// the system.
    /// </summary>
    public abstract class Person
    {
        private string firstName;
        private string lastName;
        private string email;
        private string phone;
        private DateTime dateOfBirth;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Person() { }

        /// <summary>
        /// Constructor with Parameters.
        /// </summary>
        /// <param name="firstName">Person first name</param>
        /// <param name="lastName">Person last name</param>
        /// <param name="email">Person email</param>
        /// <param name="phone">Person phone</param>
        /// <param name="birthDate">Person birth date</param>
        public Person(string firstName, string lastName, string email, string phone, DateTime birthDate)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.phone = phone;
            this.dateOfBirth = birthDate;
        }

        /// <summary>
        /// Gets or sets the first name
        /// </summary>
        public string FirstName { get => firstName; set => firstName = value; }
        /// <summary>
        /// gets or sets the last name
        /// </summary>
        public string LastName { get => lastName; set => lastName = value; }
        /// <summary>
        /// gets or sets the email
        /// </summary>
        public string Email { get => email; set => email = value; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string Phone { get => phone; set => phone = value; }

        /// <summary>
        /// gets or sets the Date of Birth
        /// </summary>
        public DateTime DateOfBirth { get => dateOfBirth; set => dateOfBirth = value; }
    }
}
