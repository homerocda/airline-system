﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// Abstract class contains common implementation for IAirpline
    /// </summary>
    public abstract class AbstractAirline : IAirline
    {
        private string name;
        private List<Employee> employees;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AbstractAirline()
        {
        }

        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="name">The name of this Airline brand.</param>
        public AbstractAirline(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Gets/sets the name of the Airline
        /// </summary>
        public string Name { get => name; set => name = value; }

        /// <summary>
        /// Gets the fleet of the airline
        /// </summary>
        public abstract List<Airplane> Fleet { get; }
        
        /// <summary>
        /// Gets / sets the list of employees of the airline
        /// </summary>
        public List<Employee> Employees { get => employees; set => employees = value; }
        
        /// <summary>
        /// Create a new employee of this Airline. The employee will have a 
        /// hiring date equal to the current date, and an empty leave date.
        /// </summary>
        /// <param name="employeeId">employee id</param>
        /// <param name="firstName">employee first name</param>
        /// <param name="lastName">employee last name</param>
        /// <param name="email">employee email</param>
        /// <param name="phone">employee phone</param>
        /// <param name="BirthDate">employee birth date</param>
        /// <param name="jobTitle">employee job title</param>
        /// <returns>the new employee</returns>
        public Employee CreateEmployee(string employeeId, string firstName, string lastName, string email, string phone, string BirthDate, string jobTitle)
        {
            throw new NotImplementedException();
        }
    }
}