﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This represnts a Ticket that can have insurance features.
    /// Decorator Pattern: this class implements a decorator around the Ticket.
    /// </summary>
    public class InsurableTicket : ITicket
    {
        private string name;
        private decimal price;
        private decimal fee;
        private List<InsuranceFeature> features;
        private Ticket ticket;

        /// <summary>
        /// gets / sets the of the Insurance type name of ticket
        /// </summary>
        public string Name { get => name; set => name = value; }

        /// <summary>
        /// get / sets the price of the ticket with insurance
        /// </summary>
        public decimal Price { get => price; set => price = value; }

        /// <summary>
        /// Gets / sets the cancelation fee for an insured ticket
        /// </summary>
        public decimal Fee { get => fee; set => fee = value; }

        /// <summary>
        /// Gets / sets the decorated ticket with flight information
        /// </summary>
        public Ticket Ticket { get => ticket; set => ticket = value; }

        /// <summary>
        /// Gets / sets the insurance features
        /// </summary>
        public List<InsuranceFeature> Features { get => features; set => features = value; }

        /// <summary>
        /// Calculate the total price based on insurance features.
        /// </summary>
        /// <returns>the ticket price</returns>
        public decimal CalculatePrice()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Cancel the ticket based on insurance features.
        /// </summary>
        public void Cancel()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the limit of hours it can be cancelled before the flight.
        /// </summary>
        /// <returns>The deadline for cancelling the ticket</returns>
        public DateTime GetCancellableHours()
        {
            throw new NotImplementedException();
        }
    }
}