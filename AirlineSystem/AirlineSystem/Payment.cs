﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// Abstract class represents a payment.
    /// Adapter patterns: Adapter classes are CreditCardPayment and BankTransferPayment
    /// Adaptees are CardPaymentService and BankService.
    /// We use the Polymorphism Grasp Pattern for this class in order to support the
    /// Adapter pattern.
    /// </summary>
    public abstract class Payment
    {
        private List<double> paymentList;

        /// <summary>
        /// Default constructor.
        /// </summary>
        protected Payment()
        {
            paymentList = new List<double>();
        }
        /// <summary>
        /// This list contains a list of amounts this payment represent.
        /// </summary>
        public List<double> PaymentList { get => paymentList; set => paymentList = value; }
        /// <summary>
        /// This abstract method represent true payment action.
        /// </summary>
        /// <param name="amount">Amount will be sent to pay.</param>
        /// <returns>Status of payment action.</returns>
        public abstract bool Pay(int amount);
    }
}