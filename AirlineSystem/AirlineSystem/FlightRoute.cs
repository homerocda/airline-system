﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// Association Class between a Route and an Airplane, created only
    /// When a flight has been assigned a Route to operate.
    /// </summary>
    public class FlightRoute
    {
        private Route route;
        private Airplane airplane;
        private DateTime departureTime;
        private DateTime arrivalTime;
        
        /// <summary>
        /// The Route this Flight Operates
        /// </summary>
        public Route Route
        {
            get
            {
                return route;
            }

            set
            {
                route = value;
            }
        }

        /// <summary>
        /// gets / sets the time of departure
        /// </summary>
        public DateTime DepartureTime { get => departureTime; set => departureTime = value; }

        /// <summary>
        /// gets / sets the time of arrival
        /// </summary>
        public DateTime ArrivalTime { get => arrivalTime; set => arrivalTime = value; }

        /// <summary>
        /// gets / sets the duration of the flight
        /// </summary>
        private TimeSpan Duration { get; set; }

        /// <summary>
        /// The airplane that will fly this route
        /// </summary>
        public Airplane Airplane
        {
            get => airplane; set => airplane = value;
        }
    }
}