﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This class defines the Category of a Seat.
    /// </summary>
    public class SeatCategory
    {
        private string name;
        private string description;
        private int categoryId;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SeatCategory()
        {
        }

        /// <summary>
        /// Constructor with parameters.
        /// </summary>
        /// <param name="name">Name of the category</param>
        /// <param name="description">Description of this seat category</param>
        /// <param name="categoryId">Identifier of the category</param>
        public SeatCategory(string name, string description, int categoryId)
        {
            Name = name;
            Description = description;
            CategoryId = categoryId;
        }

        /// <summary>
        /// Gets or sets the name of this Seat Category.
        /// </summary>
        public string Name { get => name; set => name = value; }

        /// <summary>
        /// Gets or sets the description of this Seat Category.
        /// </summary>
        public string Description { get => description; set => description = value; }

        /// <summary>
        /// Gets or sets the Identifier of this Seat Category.
        /// </summary>
        public int CategoryId { get => categoryId; set => categoryId = value; }
    }
}