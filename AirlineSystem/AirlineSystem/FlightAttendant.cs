﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This class reprents a Flight Attendant. It inherits from Person and realizes the Polymorphism.
    /// </summary>
    public class FlightAttendant : Employee
    {
        private const string JOB_TITLE = "FLIGHT_ATTENDANT";

        private List<string> duties;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public FlightAttendant()
        {
            Duties = new List<string>();
        }

        /// <summary>
        /// Constructor with parameters.
        /// </summary>
        /// <param name="firstName">flight attendant first name</param>
        /// <param name="lastName">flight attendant last name</param>
        /// <param name="email">flight attendant email</param>
        /// <param name="phone">flight attendant phone</param>
        /// <param name="birthDate">flight attendant birth date</param>
        /// <param name="employeeId">flight attendant id</param>
        /// <param name="hiringDate">flight attendant hiring date</param>
        /// <param name="leavingDate">flight attendant leave date</param>
        /// <param name="duties">flight attendant duties</param>
        public FlightAttendant(string firstName, string lastName, string email, string phone, DateTime birthDate, string employeeId, DateTime hiringDate, DateTime leavingDate) : base(firstName, lastName, email, phone, birthDate, employeeId, JOB_TITLE, hiringDate, leavingDate)
        {
            Duties = duties;
        }


        /// <summary>
        /// Gets/sets the duties of this flight attendant.
        /// </summary>
        public List<string> Duties { get => duties; set => duties = value; }
    }
}