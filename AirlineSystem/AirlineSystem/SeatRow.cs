﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This class represents a Row in the seat layout.
    /// </summary>
    public class SeatRow
    {
        private List<Seat> seats;
        private ushort rowNumber;

        /// <summary>
        /// The number of this Row.
        /// </summary>
        public ushort RowNumber { get => rowNumber; set => rowNumber = value; }

        /// <summary>
        /// The list of seats in this row.
        /// </summary>
        public List<Seat> Seats { get => seats; set => seats = value; }
    }
}