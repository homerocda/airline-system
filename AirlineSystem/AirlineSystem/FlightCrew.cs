﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// Defines the crew that will operate a flight.
    /// </summary>
    public class FlightCrew
    {
        private Pilot captain;
        private Pilot firstOfficer;
        private List<FlightAttendant> flightAttendants;

        /// <summary>
        /// Gets / sets the flight captain.
        /// </summary>
        public Pilot Captain
        {
            get => captain;
            set => captain = value;
        }

        /// <summary>
        /// Gets / sets the first officer (co-pilot)
        /// </summary>
        public Pilot FirstOfficer
        {
            get => firstOfficer;
            set => firstOfficer = value;
        }
        
        /// <summary>
        /// Gets / sets the list of flight attentants scheduled
        /// for some flight
        /// </summary>
        public List<FlightAttendant> FlightAttendants
        {
            get => flightAttendants;
            set => flightAttendants = value;
        }
    }
}