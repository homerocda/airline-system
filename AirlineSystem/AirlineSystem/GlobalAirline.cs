﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// A representation of an airline which contains brands such as Air Canada.
    /// Composite pattern is applied here, with GlobalAirline is composite class contains
    /// a list of brands (airlines).
    /// This is the Information Expert about Airlines.
    /// COMPOSITE: Contains IAirline, GlobalAirline and Airline classes.
    /// </summary>
    public class GlobalAirline : AbstractAirline
    {
        private List<Customer> customers;
        private List<Airline> brands;
        /// <summary>
        /// Default Constructor
        /// </summary>
        public GlobalAirline() : base()
        {
            brands = new List<Airline>();
        }
        /// <summary>
        /// Constructor with name parameter
        /// </summary>
        /// <param name="name">Name of the global brand</param>
        public GlobalAirline(string name) : base(name)
        {
            brands = new List<Airline>();
        }
        /// <summary>
        /// Return a global fleet contains fleets of brands.
        /// </summary>
        public override List<Airplane> Fleet
        {
            get
            {
                return brands.SelectMany( b => b.Fleet).ToList();
            }
        }

        /// <summary>
        /// Return a global list containing all flights from any Brands.
        /// </summary>
        public List<Flight> Flights
        {
            get
            {
                return brands.SelectMany(b => b.Flights).ToList();
            }
        }


        /// <summary>
        /// List of brands which airline contains
        /// </summary>
        public List<Airline> Brands { get => brands; set => brands = value; }

        /// <summary>
        /// Gets / sets the list of customers
        /// </summary>
        public List<Customer> Customers { get => customers; set => customers = value; }

        /// <summary>
        /// Creates a new Airline brand.
        /// </summary>
        /// <param name="name">name of the brand</param>
        /// <returns>the Airline representing the new Brand</returns>
        public Airline CreateBrand(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a new customer of this Airline
        /// </summary>
        /// <param name="firstName">Customer name</param>
        /// <param name="lastName">Customer surname</param>
        /// <param name="email">Customer email</param>
        /// <param name="phone">Customer phone</param>
        /// <param name="birthDate">Customer birth date</param>
        /// <param name="username">Account username</param>
        /// <param name="password">Account password</param>
        /// <returns></returns>
        public Customer CreateNewCustomer(string firstName, string lastName, string email, string phone, DateTime birthDate, string username, string password)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Search for any flight that can match the input parameters
        /// </summary>
        /// <param name="route">The route of the flight</param>
        /// <param name="departure">Departure date</param>
        /// <param name="passenger">Number of passengers that will fly</param>
        /// <param name="type">Type of ticket the customer will buy</param>
        /// <returns>List of flights matching the criteria</returns>
        public List<Flight> QueryFlightSchedule(Route route, DateTime departure, int passenger, TicketType type)
        {
            throw new NotImplementedException();
        }
    }
}