﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This class represent a creditcard payment.
    /// Adapter pattern: This class serve as adapter to adapt Credit Card service to Payment interface.
    /// It inherits from Payment and realizes the Polymorphism.
    /// </summary>
    public class CreditCardPayment : Payment
    {
        private CardPaymentService paymentService;
        private string cardInformation;
        /// <summary>
        /// Default constructor
        /// </summary>
        public CreditCardPayment()
        {
        }
        /// <summary>
        /// Default constructor with a string represent card information
        /// </summary>
        /// <param name="cardInformation">A JSON string represents card information</param>
        public CreditCardPayment(string cardInformation)
        {
            this.cardInformation = cardInformation;
        }
        /// <summary>
        /// CardPaymentService property. Represent adaptee
        /// </summary>
        public CardPaymentService CardPaymentService
        {
            get => paymentService;
            set => paymentService = value;
        }
        /// <summary>
        /// This method does the payment with fixed amount
        /// </summary>
        /// <param name="amount">Amount of money to be paid</param>
        /// <returns>true if payment is successful and false if payment is not successful.</returns>
        public override bool Pay(int amount)
        {
            paymentService.Connect();
            bool status = paymentService.Pay(amount, cardInformation);
            return status;
        }
    }
}