﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// The passenger of a particular flight. It inherits from Person and realizes the Polymorphism.
    /// </summary>
    public class Passenger : Person
    {
        private string passportNumber;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Passenger()
        {
        }

        /// <summary>
        /// Constructor with parameters.
        /// </summary>
        /// <param name="firstName">First name of the passenger</param>
        /// <param name="lastName">Last name of the passenger</param>
        /// <param name="email">Email of the passenger</param>
        /// <param name="phone">Phone of the passenger</param>
        /// <param name="birthDate">Birth date of the Passenger</param>
        /// <param name="passportNumber">Passport number of the passenger</param>
        public Passenger(string firstName, string lastName, string email, string phone, DateTime birthDate, string passportNumber) : base(firstName, lastName, email, phone, birthDate)
        {
            PassportNumber = passportNumber;
        }

        /// <summary>
        /// Gets or sets the passport number of the passenger.
        /// </summary>
        public string PassportNumber { get => passportNumber; set => passportNumber = value; }
    }
}