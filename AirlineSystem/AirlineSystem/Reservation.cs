﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    public enum ReservationStatus
    {
        ONHOLD, CANCELLED, PAID
    }
    /// <summary>
    /// This class represents a Reservation created by a single Customer.
    /// This is the Information Expert about Passengers and Tickets.
    /// </summary>
    public class Reservation
    {
        private string reservationCode;
        private List<ITicket> tickets;
        private Payment payment;
        private List<Passenger> passengers;
        private Flight ongoing;
        private Flight returning;
        private ReservationStatus status;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Reservation()
        {
            tickets = new List<ITicket>();
            passengers = new List<Passenger>();
        }

        /// <summary>
        /// Constructor with Parameters.
        /// </summary>
        /// <param name="tickets">List of tickets in this reservation.</param>
        /// <param name="payment">Payment for this reservation.</param>
        /// <param name="passengers">Passengers in this reservation.</param>
        /// <param name="ongoing">Ongoing flight of this reservation.</param>
        /// <param name="returning">Returning flight of this reservation.</param>
        /// <remarks>Reservation code is created implicitly</remarks>
        public Reservation(List<ITicket> tickets, Payment payment, List<Passenger> passengers, Flight ongoing, Flight returning)
        {
            this.tickets = tickets;
            this.payment = payment;
            this.passengers = passengers;
            this.ongoing = ongoing;
            this.returning = returning;
            // add code for creating the reservation code
        }

        /// <summary>
        /// Get or sets the payment class for this reservation.
        /// </summary>
        public Payment Payment
        {
            get => payment;
            set => payment = value;
        }

        /// <summary>
        /// Gets or sets the list of tickets in this reservation.
        /// </summary>
        public List<ITicket> Tickets { get => tickets; set => tickets = value; }


        /// <summary>
        /// Gets or sets the list of passengers in this reservation.
        /// </summary>
        public List<Passenger> Passengers { get => passengers; set => passengers = value; }

        /// <summary>
        /// Gets or sets the ongoing flight of this reservation.
        /// </summary>
        public Flight Ongoing { get => ongoing; set => ongoing = value; }

        /// <summary>
        /// Gets or sets the returning flight of this reservation.
        /// </summary>
        public Flight Returning { get => returning; set => returning = value; }

        /// <summary>
        /// gets reservation code
        /// </summary>
        public string ReservationCode { get => reservationCode; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status of reservation.</value>
        public ReservationStatus Status { get => status; set => status = value; }

        /// <summary>
        /// Creates a new passenger for this reservation.
        /// </summary>
        /// <param name="firstName">First name of the passenger</param>
        /// <param name="lastName">Last name of the passenger</param>
        /// <param name="email">Email of the passenger</param>
        /// <param name="phone">Phone of the passenger</param>
        /// <param name="birthDate">Birth date of the Passenger</param>
        /// <param name="passportNumber">Passport number of the passenger</param>
        /// <return>the new passenger attached to this reservation</return>
        public Passenger CreatePassenger(string firstName, string lastName, string email, string phone, DateTime birthDate, string passportNumber)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a new ticket for a flight in this reservation.
        /// </summary>
        /// <param name="flight">The flight of the ticket</param>
        /// <param name="passenger">Passenger attached to the ticket</param>
        /// <param name="seat">Seat attached to the ticket</param>
        /// <param name="type">Type of ticket</param>
        /// <param name="insurance">Insurance packages bought with the ticket</param>
        /// <returns></returns>
        public ITicket CreateTicket(Flight flight, Passenger passenger, Seat seat, TicketType type, List<InsuranceFeature> insurance)
        {
            throw new NotImplementedException();
        }
    }
}