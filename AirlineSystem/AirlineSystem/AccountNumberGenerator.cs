﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirlineSystem
{
    /// <summary>
    /// This class is a Pure Fabrication for generating AccountNumber for 
    /// Customers, Passengers and Employees.
    /// It should be accessed through a Singleton instance.
    /// </summary>
    public class AccountNumberGenerator
    {
        private static AccountNumberGenerator theInstance;

        /// <summary>
        /// Accesses the Singleton.
        /// </summary>
        public static AccountNumberGenerator Instance
        {
            get
            {
                if (theInstance == null)
                {
                    theInstance = new AccountNumberGenerator();
                }
                return theInstance;
            }
        }

        /// <summary>
        /// This is the main/only method of the class and it is responsible for actually generating the number
        /// </summary>
        /// <returns></returns>
        public int GenerateNumber()
        {
            int number = 0;
            // get the last number generated in a database
            // generate the new number
            return number;
        }
    }




}
