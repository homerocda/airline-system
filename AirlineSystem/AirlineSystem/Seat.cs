﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// A single seat in a Row.
    /// </summary>
    public class Seat
    {
        private string letter;
        private SeatCategory category;
        private SeatRow row;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Seat()
        {
        }

        /// <summary>
        /// Constructor with parameters.
        /// </summary>
        /// <param name="letter">The letter assigned to this Seat</param>
        /// <param name="category">The Category of this Seat</param>
        /// <param name="row">The row this seat is located</param>
        public Seat(string letter, SeatCategory category, SeatRow row)
        {
            this.letter = letter;
            this.category = category;
            this.row = row;
        }

        /// <summary>
        /// Gets or sets the letter that identifies this seat in a specific row.
        /// </summary>
        public string Letter { get => letter; set => letter = value; }

        /// <summary>
        /// Gets or sets the category of this seat.
        /// </summary>
        public SeatCategory Category { get => category; set => category = value; }

        /// <summary>
        /// Gets or sets the row this seat is located.
        /// </summary>
        public SeatRow Row { get => row; set => row = value; }
    }
}