﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// The features that can be added to an <see cref="InsurableTicket"/>
    /// </summary>
    public class InsuranceFeature
    {
        /// <summary>
        /// Gets / sets the name of the feature.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets / sets the description of this feature.
        /// </summary>
        public string Description { get; set; }
    }
}