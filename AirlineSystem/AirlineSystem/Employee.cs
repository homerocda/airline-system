﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This class represents a Airline employee. It inherits from Person and realizes the Polymorphism.
    /// </summary>
    public class Employee : Person
    {
        private string employeeId;
        private string jobTitle;
        private DateTime hiringDate;
        private DateTime leavingDate;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Employee()
        {
        }

        /// <summary>
        /// Constructor with parameters.
        /// </summary>
        /// <param name="firstName">employee first name</param>
        /// <param name="lastName">employee last name</param>
        /// <param name="email">employee email</param>
        /// <param name="phone">employee phone</param>
        /// <param name="birthDate">employee birth date</param>
        /// <param name="employeeId">employee id</param>
        /// <param name="jobTitle">job title</param>
        /// <param name="hiringDate">employee hiring date</param>
        /// <param name="leavingDate">employee leave date</param>
        public Employee(string firstName, string lastName, string email, string phone, DateTime birthDate, string employeeId, string jobTitle, DateTime hiringDate, DateTime leavingDate)
            : base(firstName, lastName, email, phone, birthDate)
        {
            this.employeeId = employeeId;
            this.jobTitle = jobTitle;
            this.hiringDate = hiringDate;
            this.leavingDate = leavingDate;
        }

        /// <summary>
        /// Gets or set the employee id.
        /// </summary>
        public string EmployeeId { get => employeeId; set => employeeId = value; }
        /// <summary>
        /// Gets/sets the hiring date.
        /// </summary>
        public DateTime HiringDate { get => hiringDate; set => hiringDate = value; }
        /// <summary>
        /// gets/sets the Leaving date.
        /// </summary>
        public DateTime LeavingDate { get => leavingDate; set => leavingDate = value; }

        /// <summary>
        /// Gets/sets the job title.
        /// </summary>
        public string JobTitle { get => jobTitle; set => jobTitle = value; }
    }
}