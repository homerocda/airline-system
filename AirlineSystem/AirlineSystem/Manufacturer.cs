﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirlineSystem
{
    /// <summary>
    /// Represents an Airplane Manufacturer.
    /// </summary>
    public class Manufacturer
    {
        private string name;
        private ExternalEmployee contact;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Manufacturer() { }

        /// <summary>
        /// Constructor with parameters.
        /// </summary>
        /// <param name="name">Name of manufacturer</param>
        /// <param name="contact">Contact person of manufacturer</param>
        public Manufacturer(string name, ExternalEmployee contact)
        {
            this.name = name;
            this.contact = contact;
        }

        /// <summary>
        /// Gets / sets the manufacturer name
        /// </summary>
        public string Name { get => name; set => name = value; }

        /// <summary>
        /// Gets / sets the contact person
        /// </summary>
        public ExternalEmployee Contact { get => contact; set => contact = value; }
    }
}
