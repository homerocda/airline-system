﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirlineSystem
{
    /// <summary>
    /// Interface represent Observer in Observer pattern.
    /// </summary>
    public interface ITicketNotifier
    {
        /// <summary>
        /// This method will be called to notify about flight changes
        /// </summary>
        /// <param name="flight">The flight that changed</param>
        void Notify(Flight flight);
    }
}
