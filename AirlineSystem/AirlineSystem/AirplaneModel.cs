﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// A specific model of airplane that an Airline can have.
    /// </summary>
    public class AirplaneModel
    {
        private List<SeatLayout> seatLayouts;

        /// <summary>
        /// gets / sets the seat layout configurations this model can have.
        /// </summary>
        public List<SeatLayout> SeatLayouts { get => seatLayouts; set => seatLayouts = value; }

        /// <summary>
        /// gets / sets the name of this model
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// gets / sets the manufacturer of this model
        /// </summary>
        public Manufacturer Manufacturer { get; set; }
    }
}