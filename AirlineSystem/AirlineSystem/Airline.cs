﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// Concrete class represent an airline implementation.
    /// It is the Information Expert about Airplanes.
    /// </summary>
    public class Airline : AbstractAirline
    {
        private List<Route> routes;
        private List<Flight> flights;
        private List<Airport> airports;
        private List<Airplane> fleet;

        /// <summary>
        /// Default constructor
        /// </summary>
        public Airline() : base()
        {
            fleet = new List<Airplane>();
        }
        /// <summary>
        /// Constructor with name as parameter
        /// </summary>
        /// <param name="name">The name of this airline.</param>
        public Airline(string name) : base(name)
        {
            fleet = new List<Airplane>();
        }
        /// <summary>
        /// Represent a list of airplanes belong to the airline
        /// </summary>
        public override List<Airplane> Fleet => fleet;

        /// <summary>
        /// Gets/sets the flights operated by this Airline
        /// </summary>
        public List<Flight> Flights { get => flights; set => flights = value; }

        /// <summary>
        /// Gets/sets the Airports this airline operates in
        /// </summary>
        public List<Airport> Airports { get => airports; set => airports = value; }

        /// <summary>
        /// Gets/sets the list of Routes this Airline can operate.
        /// </summary>
        public List<Route> Routes { get => routes; set => routes = value; }

        /// <summary>
        /// Creates a new airport this airline can fly to.
        /// </summary>
        /// <param name="name">Name of the airport</param>
        /// <param name="city">City of the Airport</param>
        /// <param name="country">Country of the airport</param>
        /// <param name="IATACode">IATA code of the airport.</param>
        /// <returns></returns>
        public Airport CreateAirport(string name, string city, string country, string IATACode)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a new route this airline operates.
        /// </summary>
        /// <param name="origin">Origin airport</param>
        /// <param name="destination">Destination airport</param>
        /// <returns></returns>
        public Route CreateRoute(Airport origin, Airport destination)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a new Airplane Model to be acquired by the airline.
        /// </summary>
        /// <param name="manufacturer">Manufacturer of the airplane</param>
        /// <param name="modelName">Airplane model name</param>
        /// <param name="layout">Seat layouts available for the Airplane</param>
        /// <returns></returns>
        public AirplaneModel CreateAirplaneModel(Manufacturer manufacturer, string modelName, List<SeatLayout> layout)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the list of airplane models this Airline has.
        /// </summary>
        /// <returns>the list of airplane models</returns>
        public List<AirplaneModel> GetAirplaneModels()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a new Airplane this Airline acquired.
        /// </summary>
        /// <param name="model">Model of the Airplane</param>
        /// <param name="callsign">Callsign</param>
        /// <param name="seatLayout">Specific seat layout of this airplane</param>
        /// <returns>The new airplane instance</returns>
        public Airplane CreateAirplane(AirplaneModel model, string callsign, SeatLayout seatLayout)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a new flight for a route, specifying its crew and departure date and time.
        /// </summary>
        /// <param name="route">Route of the flight</param>
        /// <param name="airplane">Airplane who will perform the flight</param>
        /// <param name="crew">Flight crew</param>
        /// <param name="departure">Departure date and time</param>
        /// <returns>The new flight</returns>
        public Flight CreateFlight(Route route, Airplane airplane, FlightCrew crew, DateTime departure)
        {
            throw new NotImplementedException();
        }

    }
}