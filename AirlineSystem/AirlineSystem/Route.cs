﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// Route an Airline can operate
    /// </summary>
    public class Route
    {
        private Airport toAirport;
        private Airport fromAirport;
        private float distance;

        /// <summary>
        /// Destination airport
        /// </summary>
        public Airport ToAirport { get => toAirport; set => toAirport = value; }

        /// <summary>
        /// Origin airport
        /// </summary>
        public Airport FromAirport { get => fromAirport; set => fromAirport = value; }

        /// <summary>
        /// Distance between Origin and Destination
        /// </summary>
        public float Distance { get => distance; set => distance = value; }
    }
}