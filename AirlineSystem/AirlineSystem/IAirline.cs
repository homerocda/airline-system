﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// Interface represents an airline.
    /// </summary>
    public interface IAirline
    {
        /// <summary>
        /// Name of airline
        /// </summary>
        string Name { get; }
        /// <summary>
        /// Fleet of that airline
        /// </summary>
        List<Airplane> Fleet { get; }

        /// <summary>
        /// Method responsible for adding a new employee to the Airline.
        /// </summary>
        /// <param name="employeeId">employee id</param>
        /// <param name="firstName">employee first name</param>
        /// <param name="lastName">employee last name</param>
        /// <param name="email">employee email</param>
        /// <param name="phone">employee phone</param>
        /// <param name="BirthDate">employee birth date</param>
        /// <param name="jobTitle">employee job title</param>
        /// <returns>the new employee</returns>
        Employee CreateEmployee(string employeeId, string firstName, string lastName, string email, string phone, string BirthDate, string jobTitle);
    }
}