﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// This class represents an external employee doing services
    /// for the Airline. It inherits from Person and realizes the Polymorphism.
    /// </summary>
    public class ExternalEmployee : Person
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ExternalEmployee()
        {
        }

        /// <summary>
        /// Constructor with parameters.
        /// </summary>
        /// <param name="firstName">external employee first name</param>
        /// <param name="lastName">external employee last name</param>
        /// <param name="email">external employee email</param>
        /// <param name="phone">external employee phone</param>
        /// <param name="birthDate">external employee birth date</param>
        /// <param name="employeeId">external employee id</param>
        /// <param name="hiringDate">external employee hiring date</param>
        /// <param name="leavingDate">external employee leave date</param>
        public ExternalEmployee(string firstName, string lastName, string email, string phone, DateTime birthDate) : base(firstName, lastName, email, phone, birthDate)
        {
        }
    }
}