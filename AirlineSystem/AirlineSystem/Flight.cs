﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineSystem
{
    /// <summary>
    /// Main class represent a current flight.
    /// Observer pattern: Serves as Observable in Observer pattern.
    /// It will notify all Observer in case it has something happen in FlightRoute
    /// </summary>
    public class Flight
    {
        private FlightRoute flightRoute;
        private FlightCrew flightCrew;
        private List<Ticket> tickets;
        private List<ITicketNotifier> ticketNotifiers;
        /// <summary>
        /// gets / sets the flight route
        /// </summary>
        public FlightRoute FlightRoute
        {
            get => flightRoute;
            set
            {
                flightRoute = value;
                // notify all tickets about flight route changing
                foreach(ITicketNotifier notifier in ticketNotifiers)
                {
                    notifier.Notify(this);
                }
            }
        }
        /// <summary>
        /// gets / sets the crew in flight
        /// </summary>
        public FlightCrew FlightCrew
        {
            get => flightCrew;
            set => flightCrew = value;
        }
        /// <summary>
        /// gets / sets the list of ticket in flight
        /// </summary>
        public List<Ticket> Tickets { get => tickets; set => tickets = value; }
        
        /// <summary>
        /// gets / sets the list of passengers in a flight
        /// </summary>
        public List<Passenger> Passengers
        {
            get
            {
                return tickets.Select(t => t.Passenger).ToList();
            }
        }


    }
}